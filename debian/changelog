node-xmlhttprequest (1.8.0-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Submit.
  * Remove obsolete field Name from debian/upstream/metadata (already present in
    machine-readable debian/copyright).
  * Update standards version to 4.5.0, no changes needed.

  [ Yadd ]
  * Bump debhelper compatibility level to 13
  * Add "Rules-Requires-Root: no"
  * Modernize debian/watch
    * Fix GitHub tags regex
    * Fix filenamemangle
  * Use dh-sequence-nodejs
  * Update standards version to 4.6.0, no changes needed.
  * Drop dependency to nodejs

 -- Yadd <yadd@debian.org>  Sun, 02 Jan 2022 18:39:45 +0100

node-xmlhttprequest (1.8.0-3) unstable; urgency=medium

  * Bump debhelper compatibility level to 12
  * Declare compliance with policy 4.4.0
  * Add debian/gbp.conf
  * Don't install tests in doc
  * Switch install to pkg-js-tools
  * Switch architecture to "all"

 -- Xavier Guimard <yadd@debian.org>  Sat, 03 Aug 2019 15:44:25 +0200

node-xmlhttprequest (1.8.0-2) unstable; urgency=medium

  * Remove useless link to index.js (Closes: #924165)
  * Fix Multi-Arch: foreign

 -- Xavier Guimard <yadd@debian.org>  Sun, 10 Mar 2019 10:22:24 +0100

node-xmlhttprequest (1.8.0-1) unstable; urgency=medium

  * New upstream version 1.8.0
  * Replace Mike Gabriel by me in uploaders. Thanks for your work!
    (Closes: #921369)
  * Bump debhelper compatibility version to 11
  * Declare compliance with policy 4.3.0
  * Change section to javascript
  * Change priority to optional
  * Update VCS fields to salsa
  * Update debian/copyright
  * Add upstream/metadata
  * Fix install
  * Patch source to avoid deprecation warning
  * Update homepage
  * Add tests based on pkg-js-tools
  * Add Multi-Arch: foreign

 -- Xavier Guimard <yadd@debian.org>  Mon, 04 Feb 2019 21:55:52 +0100

node-xmlhttprequest (1.6.0-1) unstable; urgency=low

  * New upstream release.
  * debian/docs:
    + Install package.json into doc folder.
  * debian/control:
    + Add nodejs to Build-Depends: field. (Closes: #725363).
    + Alioth-canonicalize Vcs-*: fields.
  * debian/copyright:
    + Add myself as copyright holder of debian/* files.
    + License change of debian/*. Use same license as upstream.

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 15 Oct 2013 23:52:16 +0200

node-xmlhttprequest (1.5.0-1) unstable; urgency=low

  [ Per Andersson ]
  * Initial release. (Closes: #640755)

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 18 May 2013 21:52:53 +0200
